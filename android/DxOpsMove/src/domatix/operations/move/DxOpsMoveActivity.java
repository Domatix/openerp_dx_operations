package domatix.operations.move;

import java.util.HashMap;
import java.util.Vector;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.*;

public class DxOpsMoveActivity extends Activity {
	OpenErpConnect oc;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        setButtonConectClickListener();
        

    }
    
    
    
    private void setButtonConectClickListener() {

        Button buttonConnect = (Button) findViewById(R.id.buttonConect);

        buttonConnect.setOnClickListener( new OnClickListener() {
			
			@Override
			public void onClick(View v) {

		    	EditText editUser = (EditText) findViewById(R.id.editUser);
		    	EditText editPassword = (EditText) findViewById(R.id.editPassword);
		        
		        //Integer port_number = 5021;
		        //String openerp_server = "178.33.160.195";
		        //String my_db = "dx_textil_test";
		    	Integer port_number = 8069;
		        String openerp_server = "192.168.1.103";
		        String my_db = "textil61test";
		        String username = editUser.getText().toString();
		        String user_pass = editPassword.getText().toString();
		        
		        oc = OpenErpConnect.connect(openerp_server, port_number, my_db, username, user_pass);
		        
		        if ( oc != null) {
			        setContentView(R.layout.reader);

			        setEditKeyPressListener();
			        ButtonProcessClickListener();
			        
		        } else {
		        	setContentView(R.layout.message);
		        }
				
			}

		    private void ButtonProcessClickListener() {

		        Button buttonProcess = (Button) findViewById(R.id.procesar);

		        buttonProcess.setOnClickListener( new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						HashMap<String, String> values = new HashMap<String, String>(); // If you need different types use HashMap<String, Object>
						//values.put("name", "partner name");
						HashMap<String, String> context = new HashMap<String, String>();
						context.put("lang", "es_ES");
						Long wizardId = oc.create("dx.operation.move.lot", values, context); // context can be null, it is not needed

						TextView textCodes = (TextView) findViewById(R.id.codes);
						String codes = textCodes.getText().toString();
						String cadena = "";
						int y = 0;
					    for (int x=0;x<codes.length();x++){
					    	if (codes.charAt(x) == '\n') {
					    		
					    		cadena = codes.substring(y, x);
					    		
								values = new HashMap<String, String>(); // If you need different types use HashMap<String, Object>
								values.put("code", cadena.toString());
								values.put("operation_move_lot_id", wizardId.toString());
								Long wizardLineId = oc.create("dx.operation.move.lot.line", values, context); // context can be null, it is not needed
								
								y = x+1;

					    	} 
					    }


			            Long[] ids = new Long[] {wizardId};
					    Boolean moveOk = (Boolean)oc.moveLot( ids,context);
					    
						setContentView(R.layout.message);

						TextView textMessage = (TextView) findViewById(R.id.message);
						
						if ( moveOk == true){
							textMessage.setText("Albarán creado");
						} else {
							textMessage.setText("Albarán no creado");
						}
						
					}
		        });
		    	
		    }
		    
			private void setEditKeyPressListener() {
		    	EditText editInput = (EditText) findViewById(R.id.input);

		    	editInput.setOnKeyListener( new OnKeyListener() {
					
					@Override
					public boolean onKey(View v, int keyCode, KeyEvent event) {
						EditText editInput = (EditText) findViewById(R.id.input);
						TextView textCodes = (TextView) findViewById(R.id.codes);
						String auxText = new String();
						 if (keyCode == KeyEvent.KEYCODE_ENTER) {
							 	auxText = textCodes.getText().toString()+editInput.getText().toString();
					            textCodes.setText(auxText);
					            editInput.setText("");
					        }

						return false;
					}
				});

		    	
		    }
		    
		});
    	
    }
    
}