# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2010 Domatix Technologies  S.L. (http://www.domatix.com) 
#                       info <info@domatix.com>
#                        Angel Moya <angel.moya@domatix.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv   

import openerp.tools as tools

class dx_operation_code(osv.osv):
    _name = 'dx.operation.code'

    _columns = {
        'name': fields.char('Name', size=64),
        'model_id': fields.many2one('ir.model', 'Model'),
        'field_id': fields.many2one('ir.model.fields', 'Field'),
    }  
    
    
    def get_code_id(self, cr, uid, model_name, code, context=None):
    
        ir_model_obj = self.pool.get("ir.model")
        
        object_obj = self.pool.get(model_name)
        
        res = ''
            
        model_ids = ir_model_obj.search(cr,uid,[('model','=',model_name)])
            
        if model_ids:
            model_id = model_ids[0]
                
            code_ids = self.search(cr,uid,[('model_id','=',model_id)])
                
            if code_ids:
                code_id = code_ids[0]
                #TODO: Codes with more than one line
                
                codebar = self.browse(cr,uid,code_id)
                    
                if codebar and codebar.name and codebar.field_id and codebar.field_id.name:
                        
                    prefix = codebar.name
                    
                    field = code[len(prefix):]
                    
                    name = codebar.field_id.name
                    
                    res = object_obj.search(cr,uid,[(name,'ilike',field),],context=context) 
                        
        return res and res[0]
        
    
    def get_code_name(self, cr, uid, model_name, context=None):
    
        ir_model_obj = self.pool.get("ir.model")
        
        res = ''
            
        model_ids = ir_model_obj.search(cr,uid,[('model','=',model_name)])
            
        if model_ids:
            model_id = model_ids[0]
                
            code_ids = self.search(cr,uid,[('model_id','=',model_id)])
                
            if code_ids:
                code_id = code_ids[0]
                    
                code = self.browse(cr,uid,code_id)
                    
                res = code and code.name
                     
        return res
            
    def operation_code_get(self, cr, uid, model_name, object_ids, context=None):
                
        ir_model_obj = self.pool.get("ir.model")
        
        object_obj = self.pool.get(model_name)
        
        res = {}
        
        if object_obj:
            
            model_ids = ir_model_obj.search(cr,uid,[('model','=',model_name)])
            
            if model_ids:
                model_id = model_ids[0]
                
                code_ids = self.search(cr,uid,[('model_id','=',model_id)])
                
                if code_ids:
                    code_id = code_ids[0]
                    
                    code = self.browse(cr,uid,code_id)
                    
                    if code and code.name and code.field_id and code.field_id.name:
                        field_name = code.field_id.name
                        for r in object_obj.read(cr, uid, object_ids,[field_name], context, load='_classic_write'):
                            prefix = code.name or ''
                            value = r[field_name] or ''
                            res[r['id']] = tools.ustr(prefix+value)

        return res
    
dx_operation_code

    