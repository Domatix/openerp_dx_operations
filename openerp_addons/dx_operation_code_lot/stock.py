# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2010 Domatix Technologies  S.L. (http://www.domatix.com) 
#                       info <info@domatix.com>
#                        Angel Moya <angel.moya@domatix.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv   

import openerp.tools as tools

class stock_production_lot(osv.osv):
    
    _inherit = 'stock.production.lot'
    
    
    def _get_operation_codebar(self, cr, uid, ids, field_name, arg, context=None):
        """ Gets stock of products for locations
        @return: Dictionary of values
        """
        
        code_obj = self.pool.get("dx.operation.code")
        
        if context is None:
            context = {}
            
        res = code_obj.operation_code_get(cr,uid,'stock.production.lot',ids,context)

        return res
    
    _columns = {
        'operation_codebar': fields.function(_get_operation_codebar, type="char", string="Code"),
    }
    
    
stock_production_lot()


class product_product(osv.osv):
    
    _inherit = 'product.product'
    
    
    def _get_operation_name(self, cr, uid, ids, field_name, arg, context=None):
        """ Gets stock of products for locations
        @return: Dictionary of values
        """
        
        if context is None:
            context = {}
        
        res = {}
        
        for product in self.browse(cr,uid,ids,context):
            name = product.descripcionarticulo or ''
            first = product.posiciondescripcionarticulo or 1000 
            if product.posicionacabado and product.posicionacabado > 0 and first > product.posicionacabado:
                first = product.posicionacabado
                name = product.acabado and product.acabado.name
            if product.posicioncomposicion and product.posicioncomposicion > 0 and first > product.posicioncomposicion:
                first = product.posicioncomposicion
                name  = product.composicion and product.composicion.name
            if product.posiciontamanyo and product.posiciontamanyo > 0 and first > product.posiciontamanyo:
                first = product.posiciontamanyo
                name  = product.size and product.size.name
            if product.posicioncolor and  product.posicioncolor > 0 and first > product.posicioncolor:
                first = product.posicioncolor
                name  = product.color and product.color.name
            if product.posiciondibujoestampado and  product.posiciondibujoestampado > 0 and first > product.posiciondibujoestampado:
                first = product.posiciondibujoestampado
                name  = product.dibujoestampado and product.dibujoestampado.name
            if product.posiciondibujojacquard and product.posiciondibujojacquard > 0 and first > product.posiciondibujojacquard:
                first = product.posiciondibujojacquard
                name  = product.dibujojacquard and product.dibujojacquard.name
            if product.posiciondibujovarios and product.posiciondibujovarios > 0 and first > product.posiciondibujovarios:
                first = product.posiciondibujovarios
                name  = product.dibujovarios and product.dibujovarios.name
            if product.posicionfamilia and product.posicionfamilia > 0 and first > product.posicionfamilia:
                first = product.posicionfamilia
                name  = product.category 
            if product.posicionsubfamilia and product.posicionsubfamilia > 0 and first > product.posicionsubfamilia:
                first = product.posicionsubfamilia
                name  = product.subcategory 
            
            if name == '':
                name = product.name or ''
                
            res[product.id] = name 
            
        return res
    
    _columns = {
        'operation_name': fields.function(_get_operation_name, type="char", string="Name",store=True),
    }
    
    
product_product()