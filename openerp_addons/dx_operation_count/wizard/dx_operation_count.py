# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2010 Domatix Technologies  S.L. (http://www.domatix.com) 
#                       info <info@domatix.com>
#                        Angel Moya <angel.moya@domatix.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
from tools.translate import _
import time
import decimal_precision as dp

    

class dx_operation_count(osv.osv_memory):
    _name = "dx.operation.count"
    
    _columns = {
        'name': fields.char('Name', size=128, required=False),
        'line_ids': fields.one2many('dx.operation.count.line', 'operation_count_id', 'Lines'),
        'result_ids': fields.one2many('dx.operation.count.result', 'operation_count_id', 'Counts'),
        'location_id': fields.many2one('stock.location','Location'),
        'picking_id':fields.many2one('stock.picking','Picking'),
        'state': fields.selection([('draft', 'New'), ('count', 'Count'), ('done', 'Done')], 'State'),
        
    }  
    
    _defaults = {
        'state': 'draft',
        }
    
    def get_lots(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        
        code_obj = self.pool.get('dx.operation.code')
        
        count_obj = self.pool.get('dx.operation.count.result')
        
        
        #Location Code
        location_code_name = code_obj.get_code_name(cr,uid,'stock.location')
        
        #Lot Code
        lot_code_name = code_obj.get_code_name(cr,uid,'stock.production.lot')
        
        #Get codes from wizard
        location_id = False
        lots = []
        
        for ops in self.browse(cr,uid,ids):
            if ops.line_ids:
                for line in ops.line_ids:
                    code = line.code
                    if code:
                        if lot_code_name == code[0:len(lot_code_name)]:
                            lots.append(code_obj.get_code_id(cr,uid,'stock.production.lot', code, context=context))
                        elif location_code_name == code[0:len(location_code_name)]:
                            if location_id:
                                #No se puede crear un conteo a dos sitios
                                return False
                            location_id = code_obj.get_code_id(cr,uid,'stock.location', code, context=context)
                            
        #Generate Stock picking
        #if location_id == False:
            #No se puede crear un movimiento sin destino
            #return False
        if lots == []:
            #No se puede crear un movimiento sin piezas
            return False
        
        
        #Generate counts
        
        for lot_id in lots:
            
            count_vals ={
                       'operation_count_id':ops.id,
                       'lot_id':lot_id,
                       }
            count_id = count_obj.create(cr,uid,count_vals)
            
        self.write(cr,uid,ops.id,{'state':'count','location_id':location_id})
   
        return True

    
    def update_lots(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        
        picking_obj = self.pool.get('stock.picking')
        
        lot_obj = self.pool.get('stock.production.lot')
        
        move_obj = self.pool.get('stock.move')
        lots = []
        
        for ops in self.browse(cr,uid,ids):
            if ops.result_ids:
                for line in ops.result_ids:
                    if line.lot_id:
                        lots.append(line.lot_id)
        
            if ops.location_id == False:
                #No se puede crear un conteo sin destino
                return False
            if lots == []:
                #No se puede crear un movimiento sin piezas
                return False
            
        
            picking_vals ={
                           'location_dest_id':ops.location_id and ops.location_id.id,
                           'type':'internal'
                           }
            
            picking_id = picking_obj.create(cr,uid,picking_vals)
            
            
            #Generate Stock moves
            
            for line in ops.result_ids:
                
                move_vals ={
                           'location_dest_id':ops.location_id and ops.location_id.id,
                           'location_id':line.lot_id.location_id.id,
                           'product_id':line.lot_id.product_id.id,
                           'product_uom': line.lot_id.product_id.uom_id.id,
                           'product_qty':line.stock - line.count,
                           #'product_uom':line.lot_id.product_id and line.lot_id.product_id.uom_id.id,
                           'picking_id':picking_id,
                           'prodlot_id':line.lot_id.id,
                           'name': line.lot_id.product_id.partner_ref
                           }
                move_id = move_obj.create(cr,uid,move_vals)
            
            
            
                self.write(cr,uid,ops.id,{'state':'done','picking_id':picking_id})
                    
        return True

dx_operation_count()


class dx_operation_count_line(osv.osv_memory):
    _name = "dx.operation.count.line"
    
    _columns = {
        'operation_count_id': fields.many2one('dx.operation.count', 'Operation'),
        'code': fields.char('Code', size=128, required=False)
    }  
    _rec_name = "code"
    
    
dx_operation_count_line()

class dx_operation_count_result(osv.osv_memory):
    _name = "dx.operation.count.result"
    
    _columns = {
        'operation_count_id': fields.many2one('dx.operation.count', 'Operation'),
        'lot_id': fields.many2one('stock.production.lot', 'Lot'),
        'product_id': fields.related('lot_id','product_id',type="many2one",relation="product.product",string="Product"),
        'stock': fields.related('lot_id','stock_available',type="float",string="Stock"),
        'count': fields.float('Count', digits_compute=dp.get_precision('Product UoM')),        
    }  
    _rec_name = "lot_id"
    
dx_operation_count_result()