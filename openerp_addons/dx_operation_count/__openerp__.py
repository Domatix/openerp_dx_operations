# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2010 Domatix Technologies  S.L. (http://www.domatix.com) 
#                       info <info@domatix.com>
#                        Angel Moya <angel.moya@domatix.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
	"name" : "DX Warehouse Operations - Count",
	"version" : "1.6",
	"author" : "Domatix",
	"website": "www.domatix.com",
	"category" : "Warehouse",
	"depends" : ["web","dx_operation_code_lot","dx_operation_code_location"],
        "init_xml" : [],
        "demo_xml" : [],
        "description": """DX Warehouse Operations - Count""",
        "update_xml" : [
					'wizard/dx_operation_count_view.xml',
					'dx_operation_count_view.xml',
					],
    "active": False,
    "installable": True
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
