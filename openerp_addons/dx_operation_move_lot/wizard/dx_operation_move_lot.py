# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2010 Domatix Technologies  S.L. (http://www.domatix.com) 
#                       info <info@domatix.com>
#                        Angel Moya <angel.moya@domatix.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
from tools.translate import _
import time

    

class dx_operation_move_lot(osv.osv_memory):
    _name = "dx.operation.move.lot"
    
    _columns = {
        'name': fields.char('Name', size=128, required=False),
        'picking_id':fields.many2one('stock.picking','Picking'),
        'line_ids': fields.one2many('dx.operation.move.lot.line', 'operation_move_lot_id', 'Lines'),
        'state': fields.selection([('draft', 'New'), ('done', 'Done')], 'State'),
    }  
    
    _defaults = {
        'state': 'draft',
        }
    
    def move_lot(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        
        models_data = self.pool.get('ir.model.data')
        
        code_obj = self.pool.get('dx.operation.code')
        
        picking_obj = self.pool.get('stock.picking')
        
        lot_obj = self.pool.get('stock.production.lot')
        
        move_obj = self.pool.get('stock.move')
        
        #Location Code
        location_code_name = code_obj.get_code_name(cr,uid,'stock.location')
        
        #Lot Code
        lot_code_name = code_obj.get_code_name(cr,uid,'stock.production.lot')
        
        #Get codes from wizard
        location_id = False
        lots = []
        
        for ops in self.browse(cr,uid,ids):
            if ops.line_ids:
                for line in ops.line_ids:
                    code = line.code
                    if code:
                        if lot_code_name == code[0:len(lot_code_name)]:
                            lots.append(code_obj.get_code_id(cr,uid,'stock.production.lot', code, context=context))
                        elif location_code_name == code[0:len(location_code_name)]:
                            if location_id:
                                #No se puede crear un movimiento a dos sitios
                                return False
                            location_id = code_obj.get_code_id(cr,uid,'stock.location', code, context=context)
                            
        #Generate Stock picking
        if location_id == False:
            #No se puede crear un movimiento sin destino
            return False
        if lots == [] or lots == [[]]:
            #No se puede crear un movimiento sin piezas
            return False
        
        picking_vals ={
                       'location_dest_id':location_id,
                       'type':'internal'
                       }
        
        picking_id = picking_obj.create(cr,uid,picking_vals)
        
        #Generate Stock moves
        
        for lot_id in lots:
            lot = lot_obj.read(cr,uid,lot_id,['stock_available','location_id','product_id'])
            
            product = self.pool.get('product.product').browse(cr, uid, [lot['product_id'][0]], context=context)[0]
            
            move_vals ={
                       'location_dest_id':location_id,
                       'location_id':lot['location_id'][0],
                       'product_id':lot['product_id'][0],
                       'product_uom': product.uom_id.id,
                       'product_qty':lot['stock_available'],
                       #'product_uom':lot.product_id and lot.product_id.uom_id.id,
                       'picking_id':picking_id,
                       'prodlot_id':lot_id,
                       'name': product.partner_ref
                       }
            move_id = move_obj.create(cr,uid,move_vals)
            
        
        self.write(cr,uid,ops.id,{'state':'done','picking_id':picking_id})
        
        return True


dx_operation_move_lot()


class dx_operation_move_lot_line(osv.osv_memory):
    _name = "dx.operation.move.lot.line"
    
    _columns = {
        'operation_move_lot_id': fields.many2one('dx.operation.move.lot', 'Operation'),
        'code': fields.char('Code', size=128, required=False),
    }  
    _rec_name = "code"
    
dx_operation_move_lot_line()