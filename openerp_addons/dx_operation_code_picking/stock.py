# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2010 Domatix Technologies  S.L. (http://www.domatix.com) 
#                       info <info@domatix.com>
#                        Angel Moya <angel.moya@domatix.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv   

import openerp.tools as tools

class stock_picking(osv.osv):
    
    _inherit = 'stock.picking'
    
    
    def _get_operation_codebar(self, cr, uid, ids, field_name, arg, context=None):
        """ Gets stock of products for locations
        @return: Dictionary of values
        """
        
        code_obj = self.pool.get("dx.operation.code")
        
        if context is None:
            context = {}
            
        res = code_obj.operation_code_get(cr,uid,'stock.picking',ids,context)

        return res
    
    
    _columns = {
        'operation_codebar': fields.function(_get_operation_codebar, type="char", string="Code"),
    }
    
    
stock_picking()