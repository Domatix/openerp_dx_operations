# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2010 Domatix Technologies  S.L. (http://www.domatix.com) 
#                       info <info@domatix.com>
#                        Angel Moya <angel.moya@domatix.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
from tools.translate import _
import time

    

class dx_operation_picking_lot_assign(osv.osv_memory):
    _name = "dx.operation.picking.lot.assign"
    
    _columns = {
        'name': fields.char('Name', size=128, required=False),
        'picking_id':fields.many2one('stock.picking','Picking'),
        'line_ids': fields.one2many('dx.operation.picking.lot.assign.line', 'operation_picking_lot_assign_id', 'Lines'),
        'state': fields.selection([('draft', 'New'), ('done', 'Done')], 'State'),
    }  
    
    _defaults = {
        'state': 'draft',
        }
    
    def lot_assign(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        
        models_data = self.pool.get('ir.model.data')
        
        code_obj = self.pool.get('dx.operation.code')
        
        picking_obj = self.pool.get('stock.picking')
        
        lot_obj = self.pool.get('stock.production.lot')
        
        move_obj = self.pool.get('stock.move')
        
        #Picking Code
        picking_code_name = code_obj.get_code_name(cr,uid,'stock.picking')
        
        #Lot Code
        lot_code_name = code_obj.get_code_name(cr,uid,'stock.production.lot')
        
        #Get codes from wizard
        picking_id = False
        lots = []
        
        for ops in self.browse(cr,uid,ids):
            if ops.line_ids:
                for line in ops.line_ids:
                    code = line.code
                    if code:
                        if lot_code_name == code[0:len(lot_code_name)]:
                            lots.append(code_obj.get_code_id(cr,uid,'stock.production.lot', code, context=context))
                        elif picking_code_name == code[0:len(picking_code_name)]:
                            if picking_id:
                                #No se puede crear un movimiento a dos sitios
                                return False
                            picking_id = code_obj.get_code_id(cr,uid,'stock.picking', code, context=context)
                            
        #Update Stock picking
        if picking_id == False or picking_id == []:
            #No se puede actualizar un picking si no se indica
            return False
        if lots == []:
            #No se puede actualizar un picking sin o se indican sin piezas
            return False
        
        
        
        #Generate Stock moves
        
        for lot_id in lots:
            lot = lot_obj.read(cr,uid,lot_id,['stock_available','location_id','product_id'])
            
            move_ids = move_obj.search(cr,uid,[('picking_id','=',picking_id),('product_id','=',lot['product_id'][0])]) 

            if not (move_ids and move_ids[0]):
                #No se puede actualizar un picking con piezas que no corresponden
                return False
            
            product = self.pool.get('product.product').browse(cr, uid, [lot['product_id'][0]], context=context)[0]
            
            move_vals ={
                       'location_id':lot['location_id'][0],
                       'product_qty':lot['stock_available'],
                       #'product_uom':lot.product_id and lot.product_id.uom_id.id,
                       'picking_id':picking_id,
                       'prodlot_id':lot_id,
                       }
            
            move_obj.write(cr,uid,move_ids[0],move_vals)
        
        self.write(cr,uid,ops.id,{'state':'done','picking_id':picking_id})
        
        return True


dx_operation_picking_lot_assign()


class dx_operation_picking_lot_assign_line(osv.osv_memory):
    _name = "dx.operation.picking.lot.assign.line"
    
    _columns = {
        'operation_picking_lot_assign_id': fields.many2one('dx.operation.picking.lot.assign', 'Operation'),
        'code': fields.char('Code', size=128, required=False),
    }  
    _rec_name = "code"
    
dx_operation_picking_lot_assign()